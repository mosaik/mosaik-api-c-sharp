using mosaik_api_c_sharp.API;

namespace UnitTests
{
    [TestClass]
    public class LoggerTests
    {
        [TestMethod]
        public void IsValidLogger_NotNull() 
        {
            Logger logger = new Logger();
            Assert.IsNotNull(logger);
        }

        [TestMethod]
        public void IsValidFile_CreatedSuccessful() 
        {
            Logger logger = new Logger();
            logger.CreateNewFile();
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void IsValidPath() 
        {
            Logger logger = new Logger();
            string path = logger.logFilePath;
            bool isPathValid = File.Exists(path);
            Assert.IsTrue(isPathValid);
        }

        [TestMethod]
        public void IsValidFile_WrittingSuccessful() 
        {
            Logger logger = new Logger();
            logger.Log("write to log file");
            Assert.IsTrue(true);
        }
    }
}