using mosaik_api_c_sharp.API;

namespace UnitTests
{
    [TestClass]
    public class ExtensionTests
    {
        [TestMethod]
        public void IsValidByteArray_IsNotNull()
        {
            string stringToByteArray = "message";
            byte[] byteArray = Extension.ToByteArray(stringToByteArray);
            Assert.IsNotNull(byteArray);
        }

        [TestMethod]
        public void IsValidByteArray_IsValidContent()
        {
            string stringToByteArray = "message";
            byte[] byteArray = Extension.ToByteArray(stringToByteArray);
            System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
            string byteArrayToString = enc.GetString(byteArray);
            Assert.AreEqual(byteArrayToString, stringToByteArray);
        }
    }
}