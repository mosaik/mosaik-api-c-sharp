﻿using mosaik_api_c_sharp.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace IntegrationTests
{
    /// <summary>
    /// This class is an example how to handle the messages for the communication with mosaik.
    /// It serves also as an integration test.
    /// </summary>
    public class ExampleMAS : Simulator
    {
        private long stepSize;
        private int idCounter = 0;

        private ExampleAgent agent;

        private JObject CreateInitMessage(string apiVersion)
        {
            return (JObject)JValue.Parse(("{"
                + "    'api_version': " + apiVersion + ","
                + "    'models': {"
                + "        'ExampleAgent': {" + "            'public': true,"
                + "            'params': []," + "            'attrs': []"
                + "    }" + "    }" + "}").Replace("'", "\""));
        }

        public ExampleMAS(string[] args)
            : base()
        {
            this.agent = null;
            this.simName = args[2];
            this.apiVersion = args[1];
            this.stepSize = long.Parse(args[3]);
        }

        #pragma warning disable
        public override JObject init(String sid, JObject simParams)
        {
            if (simParams["step_size"] != null)
            {
                this.stepSize = (int)simParams["step_size"];
            }
            return CreateInitMessage(this.apiVersion);
        }
        #pragma warning restore

        #pragma warning disable
        public override JArray create(int num, String model,
                JObject modelParams)
        {
            if (this.agent != null || num > 1)
            {
                throw new Exception("Can only create one agent. :-(");
            }

            this.agent = new ExampleAgent();

            JObject entity = new JObject();
            entity.Add("eid", "EA_0");
            entity.Add("type", model);
            entity.Add("rel", new JArray());

            JArray entities = new JArray();
            entities.Add(entity);

            return entities;
        }
        #pragma warning restore

        #pragma warning disable
        public override void setupDone() { }
        #pragma warning restore

        #pragma warning disable
        public override long step(long time, JObject inputs){
            MosaikProxy mosaik = this.mosaik;

            // Get simulation progress in percent
            float progress = mosaik.getProgress();
            ConsoleWriter.WriteLine("Progress: " + progress);

            // Show various ways of getting related entities
            object rels = mosaik.getRelatedEntities();
            ConsoleWriter.WriteLine("All relations: " + rels);
            rels = mosaik.getRelatedEntities("eid0");
            ConsoleWriter.WriteLine("Relations for eid0: " + rels);
            rels = mosaik.getRelatedEntities(new String[] { "eid0", "eid1" });
            ConsoleWriter.WriteLine("Relations for {eid0, eid1}: " + rels);

            // Get another simulator's data from mosaik
            JObject attrs = new JObject();
            JArray input = new JArray();
            input.Add("a");
            attrs.Add("eid0", input);
            Object data = mosaik.getData(attrs);
            // Set input data for another simulator
            string serializedObject = JsonConvert.SerializeObject(data);
            JObject inputData = new JObject();
            inputData.Add("EA_0", JObject.Parse(serializedObject));

            mosaik.setData(inputData);

            this.agent.step();

            return time + this.stepSize;
        }
        #pragma warning restore

        public override JObject getData(JObject outputs)
        {
            // Nothing to give ...
            JObject data = new JObject();
            return data;
        }

        public override void stop()
        {
            Logger logger = new Logger();
            logger.Log("Message: stop message handling!");
        }
    }
}

class ExampleAgent
{
    public ExampleAgent()
    {
    }

    public void step()
    {
    }
}
