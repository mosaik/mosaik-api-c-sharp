﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace mosaik_api_c_sharp.API
{
    /// <summary>
    /// This class is a proxy to mosaik that allows you to query it for simulation
    /// or simulator data.
    /// </summary>
    public class MosaikProxy
    {
        //private TcpHandling sock;
        private SimpyIoSocket simSock;

        /// <summary>
        /// Create a proxy object for a simpy.io socket.
        /// </summary>
        /// <param name="sock">sock is the simpy.io socket connected to mosaik</param>
        public MosaikProxy(SimpyIoSocket sock)
        {
            this.simSock = sock;
        }

        /// <summary>
        /// Get the current simulation progress.
        /// </summary>
        /// <returns>the simulation progress in per-cent</returns>
        public float getProgress()
        {
            try
            {
                return Convert.ToSingle(this.simSock.makeRequest("get_progress"));
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get the complete entity graph, e.g.:
        /// <pre>{
        ///     'nodes': {
        ///         'sid_0.eid_0': {'type': 'A'},
        ///         'sid_0.eid_1': {'type': 'B'},
        ///         'sid_1.eid_0': {'type': 'C'},
        ///     },
        ///     'edges': [
        ///         ['sid_0.eid_0', 'sid_1.eid0', {}],
        ///         ['sid_0.eid_1', 'sid_1.eid0', {}],
        ///     ],
        /// }</pre>
        /// </summary>
        /// <returns>the complete entity graph</returns>
#pragma warning disable
        public object getRelatedEntities()
        {
            return this.simSock.makeRequest("get_related_entities");
        }
#pragma warning restore

        /// <summary>
        /// Get all entities related to another entity <em>fullId</em>, e.g.:
        /// <pre>{
        ///     'sid_0.eid_0': {'type': 'A'},
        ///     'sid_0.eid_1': {'type': 'B'},
        /// }</pre>
        /// </summary>
        /// <param name="fullId">the full entity ID for which to query related entities</param>
        /// <returns>a map of related entity IDs</returns>
#pragma warning disable
        public object getRelatedEntities(String fullId)
        {
            JArray args = new JArray();
            args.Add(fullId);
            return this.simSock.makeRequest("get_related_entities", args);
        }
#pragma warning restore

        /// <summary>
        /// Get all entities related to a list of other entities, e.g.:
        /// <pre>{
        ///     'sid_0.eid_0': {
        ///         'sid_0.eid_1': {'type': 'B'},
        ///     },
        ///     'sid_0.eid_1': {
        ///         'sid_0.eid_1': {'type': 'B'},
        ///     },
        /// }</pre>
        /// </summary>
        /// <param name="fullIds">a list of full entity IDs</param>
        /// <returns>a map of related entitis</returns>
#pragma warning disable
        public object getRelatedEntities(String[] fullIds)
        {
            JArray args = new JArray();
            args.Add(fullIds.OfType<string>().ToList());
            JArray array = new JArray();
            array.Add(args);
            return this.simSock.makeRequest("get_related_entities", array);
        }
#pragma warning restore

       /// <summary>
       /// Return the data for the requested attributes <em>attrs</em>.
       /// 
       /// 'attrs' is a dict of (fully qualified) entity IDs mapping to lists of
       /// attribute names (``{'sid/eid': ['attr1', 'attr2']}``).
       /// 
       /// The return value is a dict mapping the input entity IDs to data
       /// dictionaries mapping attribute names to there respective values
       /// (``{'sid/eid': {'attr1': val1, 'attr2': val2}}``).
       /// 
       /// </summary>
       /// <param name="attrs">
       /// a map of (fully qualified) entity IDs mapping to lists
       /// of attribute names:
       /// <code>{'sid/eid': ['attr1', 'attr2']}</code>
       /// </param>
       /// <returns>
       /// a map that maps the input entity IDs to data maps that with
       /// attributes and values:
       /// <code>{'sid/eid': {'attr1': val1, 'attr2': val2}}</code>
       /// </returns>
#pragma warning disable
        public Object getData(JObject attrs)
        {
            JArray args = new JArray();
            args.Add(attrs);
            return this.simSock.makeRequest("get_data", args);
        }
#pragma warning restore

        /// <summary>
        /// Set <em>data</em> as input data for all affected simulators.
        /// </summary>
        /// <param name="data">
        /// a map mapping source entity IDs to destination entity IDs with maps of attributes and values:
        /// <code>{'src_full_id': {'dest_full_id':
        ///      {'attr1': 'val1', 'attr2': 'val2'}}}</code>
        /// </param>
#pragma warning disable
        public void setData(JObject data)
        {
            JArray args = new JArray();
            args.Add(data);
            this.simSock.makeRequest("set_data", args);
        }
#pragma warning restore
    }
}
