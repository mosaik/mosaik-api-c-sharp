﻿using System.Text;

namespace mosaik_api_c_sharp.API
{
    public static class Messages
    {
        /// <summary>
        /// Creates the Type of Plant (or several Types of Plants)
        /// </summary>
        public static string CreateInitResponse(string apiVersion)
        {
            string message = "{\"api_version\": '" + apiVersion + "', " +
                "\"models\": {" +
                    "\"PV\": {" +
                        "\"public\": true," +
                        "\"params\": []," +
                        "\"attrs\": [\"P\"]" +
                    "}" +
               "}}";
            return message;
        }

        /// <summary>
        /// MEssage with list of instances
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static string CreateCreateResponse(string model)
        {
            string message = "[" +
                        "{" +
                            "\"eid\": \"pv_0\"," +
                            "\"type\": \"PV\"," +
                            "\"rel\": []," +
                            "\"children\": [" +
                                //"{" +
                                //    "\"eid\": \"pv_0\"," +
                                //    "\"type\": \"Node\"," +
                                //"}," +
                                //"{" +
                                //    "\"eid\": \"pv_1\"," +
                                //    "\"type\": \"Node\"," +
                                //"}," +
                            "]" +
                        "}" +
                    "]";
            return message;
        }

        /// <summary>
        /// Message with current value
        /// </summary>
        /// <param name="itemName"></param>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string CreateGetDataResponse(string itemName, string type, float value) 
        {
            string message = "{\"" + itemName + "\": " +
                                "{\"" +
                                    type + "\": " + value.ToString(System.Globalization.CultureInfo.CreateSpecificCulture("en-US")) +
                                 "}" +
                              "}";
            return message;
        }

    }
}
