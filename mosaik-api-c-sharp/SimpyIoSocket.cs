﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using mosaik_api_c_sharp.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace mosaik_api_c_sharp.API
{
    /// <summary>
    /// This class allows you to communicate with simpy.io sockets.
    /// It provides methods at two abstraction levels. <code>send()</code> and
    /// <code>recv()</code> work at a rather low level. They de-/encode and transmit
    /// JSON message objects. <code>makeRequest()</code> and
    /// <code>recvRequest()</code> provide a higher level abstraction and keep
    /// track of message IDs and that stuff that you don't want to care about.
    /// </summary>
    public class SimpyIoSocket : IDisposable
    {
        public string apiVersion { get; set; }
        public string simulationName { get; set; }
        public long stepSize { get; set; }
        private int outMsgId;
        private static BufferedStream bufferedStream;
        private static NetworkStream stream;
        public static bool isServer = false;
        private TcpClient client;
        private TcpListener server;
        private Logger logger = new Logger();

        /// <summary>
        /// Object to set the type of the message.
        /// </summary>
        public class MsgType
        {
            public static int REQ = 0;
            public static int SUCCESS = 1;
            public static int ERROR = 2;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resources
                Close();
            }
            // free native resources
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="addr">the address to connect to (<em>host:port</em>)</param>
        public SimpyIoSocket(String addr)
        {
            string[] parts = addr.Split(':');
            string host = parts[0];
            int port = int.Parse(parts[1]);
            if (isServer) {
	            logger.Log("Listening on " + host + ":" + port);
                ConsoleWriter.WriteLine("Started as server!");
                ConsoleWriter.WriteLine("Listening on " + host + ":" + port);
	            this.server = new TcpListener(IPAddress.Parse(host), port);
                server.Start();
	            this.client = server.AcceptTcpClient();
                logger.Log("Simulation server connected to " + host + ":" + port);
            }
            else {
                logger.Log("Connecting to " + host + ":" + port);
                ConsoleWriter.WriteLine("Started as client!");
                ConsoleWriter.WriteLine("Connecting to " + host + ":" + port);
        	    this.server = null;
                this.client = new TcpClient(host, port);
                logger.Log("Simulation client connected to " + host + ":" + port);
            }

            this.outMsgId = 0;
            stream = client.GetStream();
            bufferedStream = new BufferedStream(stream, 4096);
        }

        /// <summary>
        /// Close the socket.
        /// </summary>
        public void Close() {

            if (isServer)
            {
                this.server.Stop();
            }
            this.client.Close();
        }

        #region Request
        /// <summary>
        /// Request object
        /// </summary>
        public class Request
        {
            private SimpyIoSocket sock;
            private int msgId;

            /// <summary>
            /// The method name the sender wants to call on your side.
            /// </summary>
            public String method;

            /// <summary>
            /// The arguments for that method call.
            /// </summary>
            public JArray args;

            /// <summary>
            /// The keyword arguments for that method call.
            /// </summary>
            public JObject kwargs;

            /// <summary>
            /// Sets all the needed information for the request.
            /// </summary>
            /// <param name="sock"> the socket instance that received the request</param>
            public Request(SimpyIoSocket sock)
            {
                try
                {
                    this.sock = sock;
                    JArray payload = recv();

                    // Expand payload
                    int msgType = ((int)payload.ElementAt(0));
                    if (msgType != MsgType.REQ)
                    {
                        throw new IOException("Expected message type 0, got " + msgType);
                    }
                    this.msgId = ((int)payload.ElementAt(1));
                    JArray call = (JArray)payload.ElementAt(2);

                    // Set request data
                    this.method = (String)call.ElementAt(0);
                    this.args = (JArray)call.ElementAt(1);
                    this.kwargs = (JObject)call.ElementAt(2);
                }
                catch (Exception)
                {
                    throw;
                }
            }

            /// <summary>
            /// Send a reply to this request passing a <em>result</em>.
            /// </summary>
            /// <param name="result">the return value of the method call</param>
#pragma warning disable
            public void reply(JObject result)
            {
                try
                {
                    JArray reply = new JArray();
                    reply.Add(MsgType.SUCCESS);
                    reply.Add(this.msgId);
                    reply.Add(result);
                    send(reply);
                }
                catch (Exception)
                {
                    throw;
                }
            }
#pragma warning restore

            /// <summary>
            /// result message with long type 
            /// </summary>
            /// <param name="result"></param>
#pragma warning disable
            public void reply(long result)
            {
                try
                {
                    JArray reply = new JArray();
                    reply.Add(MsgType.SUCCESS);
                    reply.Add(this.msgId);
                    reply.Add(result);
                    send(reply);
                }
                catch (Exception)
                {
                    throw;
                }
            }
#pragma warning restore

            /// <summary>
            /// result message with JArray type 
            /// </summary>
            /// <param name="result"></param>
#pragma warning disable
            public void reply(JArray result)
            {
                try
                {
                    JArray reply = new JArray();
                    reply.Add(MsgType.SUCCESS);
                    reply.Add(this.msgId);
                    reply.Add(result);
                    send(reply);
                }
                catch (Exception)
                {
                    throw;
                }
            }
#pragma warning restore
        }
        #endregion Request

        #region Make Request
        /// <summary>
        /// create a request message with only a string input
        /// </summary>
        /// <param name="method">the message handle method</param>
        /// <returns>message object</returns>
        public Object makeRequest(String method)
        {
            try
            {
                return this.makeRequest(method, new JArray(), new JObject());
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// create a request message with the method-name for the message handling and 
        /// the arguments in a Json array 
        /// </summary>
        /// <param name="method">the method-name for the message handling</param>
        /// <param name="args">the arguments in a Json array</param>
        /// <returns>>message object</returns>
        public JToken makeRequest(String method, JArray args)
        {
            try
            {
                return this.makeRequest(method, args, new JObject());
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// create a request message with the method-name for the message handling,
        /// the arguments in a Json array and a dictionary of arguments
        /// </summary>
        /// <param name="method">the method-name for the message handling</param>
        /// <param name="args">the arguments in a Json array</param>
        /// <param name="kwargs">a dictionary of arguments</param>
        /// <returns>>message object</returns>
#pragma warning disable
        public JToken makeRequest(String method, JArray args, JObject kwargs)
        {
            try
            {
                // Construct content list
                JArray call = new JArray();
                call.Add(method);
                call.Add(args);
                call.Add(kwargs);

                // Construct payload
                JArray req = new JArray();
                req.Add(MsgType.REQ);
                req.Add(this.outMsgId);
                req.Add(call);

                // Make request, wait for reply and return the payload's content
                send(req);
                JArray reply = recv();
                return reply.ElementAt(2);
            }
            catch (Exception)
            {
                throw;
            }
        }
#pragma warning restore
        #endregion Make Request

        #region Receive / Send
        /// <summary>
        /// Create a Request object out of the received message
        /// </summary>
        /// <returns>Request object</returns>
        public Request recvRequest()
        {
            try
            {
                return new Request(this);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Send the message via a TCP-Socket
        /// </summary>
        /// <param name="message">Json Array with the message included</param>
        public static void send(JArray message)
        {
            try
            {
                Logger logger = new Logger();
                logger.Log("Start sending data");
                // Translate the passed message into ASCII and store it as a Byte array.
                Byte[] data = ((string)JsonConvert.SerializeObject(message)).ToByteArray();

                // Send the message to the connected TcpServer. 
                byte[] header = BitConverter.GetBytes(data.Length);
                if (BitConverter.IsLittleEndian)
                    Array.Reverse(header);
                stream.Write(header, 0, 4);
                stream.Write(data, 0, data.Length);

                ConsoleWriter.WriteLine("Sent: " + message);
                logger.Log("Sent: " + message);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Receive the TcpServer.response.
        /// </summary>
        /// <returns>Json Array with the message</returns>
        public static JArray recv()
        {
            try
            {
                Logger logger = new Logger();
                logger.Log("Start receiving data");
                // Buffer to store the response bytes.
                Byte[] data = new Byte[256];

                int size;
                int read;
                int res;
                byte[] header = new byte[4];
                

                // Read header
                read = 0;
                size = 4;
                while (read < size)
                {
                    res = bufferedStream.Read(header, read, size - read);
                    if (res < 0)
                    {
                        throw new IOException("Unexpected end of stream");
                    }
                    read += res;
                }

                // Read content
                if (BitConverter.IsLittleEndian)
                    Array.Reverse(header);
                read = 0;
                size = BitConverter.ToInt32(header, 0);
                logger.Log("message size = " + size);

                data = new byte[size];
                while (read < size)
                {
                    res = bufferedStream.Read(data, read, size - read);
                    if (res < 0)
                    {
                        throw new IOException("Unexpected end of stream");
                    }
                    read += res;
                }
                               
                String message = Encoding.ASCII.GetString(data, 0, data.Length);
                logger.Log("received message: " + message );
                JArray payload = new JArray();
                try
                {
                    payload = JArray.Parse(message);
                }
                catch(Exception)
                {
                    logger.Log("Payload problem in message: " + message);
                    throw;
                }
                ConsoleWriter.WriteLine("Received: " + message);
                logger.Log("Received: " + payload);
                return payload;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion Receive / Send
    }
}
